/*
Copyright 2009 Thaya Kareeson (email : thaya.kareeson@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Version History:

1.1 - 01/08/2009
- Floating message box
- Content is no longer hidden

1.0 - 01/06/2009
- Initial release
*/

function adbm_get_cookie(c_name) {
  if (document.cookie.length>0) {
    c_start=document.cookie.indexOf(c_name + "=");
    if (c_start!=-1) {
      c_start=c_start + c_name.length+1;
      c_end=document.cookie.indexOf(";",c_start);
      if (c_end==-1) c_end=document.cookie.length;
      return unescape(document.cookie.substring(c_start,c_end));
    }
  }
  return "";
}

function adbm_set_cookie(c_name,value,expiredays) {
  var exdate = new Date();
  exdate.setDate(exdate.getDate()+expiredays);
  document.cookie=c_name+ "=" +escape(value)+";path="+"/"+
  ((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
}

// run when document is ready
jQuery(document).ready(function($){
  adbm_cookie = adbm_get_cookie("adbm");
  if(adbm_cookie == ""){
    // set cookie to make sure the message doesnt show up again.
    adbm_set_cookie('adbm', '1', 365);
    // check if adblock is on
    if($("#banner").css("display") == "none"){
      $("#content_adblock_message").css({
        "background":"#fff",
        "border":"1px solid #ccc",
        "height":"auto",
        "left":"25%",
        "padding":"20px",
        "position":"fixed",
        "top":"100px",
        "width":"50%",
        "z-index":"10"
      }).html("<img src='http://omninoggin.com/wp-content/uploads/2009/01/disable_adblock.png' alt='How to disable Adblock' class='alignright' style='border:1px solid #ccc;margin:0 0 0 20px;padding:5px'/><div><h3 style='margin:0 0 20px 0'>Doh! It appears that you are using AdBlock software.</h3><p>Without advertisement revenue we will not be able to continue to provide quality content.  <strong>Plese consider disabling your AdBlock software (at least for this site)</strong>.  Thank you in advance for your support and understanding!</p><br/><p align='center'><a id='close-adblock-message' href='#' rel='nofollow'>Click here to close</a><br/>You will not receive it again during this session (cookies required).</p></div>").hide().fadeIn();
    }

    // bind close action to adblock message
    $("#close-adblock-message").click(function(){
      $("#content_adblock_message").fadeOut();
    });
  }
});
